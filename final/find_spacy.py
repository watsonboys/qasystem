#!/usr/bin/python3

import spacy

def find_spacy(line, nlp):

    '''this function makes use of the spacy package, specifically made use of the dependency (.dep_)
    attribute of this package to find dependencies between words.'''
    verb = ['use','come','be','can',
        'could','do','have','may','might','must',
            'shall','would','should','will']
    ent = []
    subject = [] 
    result = nlp(line)
    for w in result:
        if w.tag_.startswith('NN') or w.tag_ == 'VBN' or w.tag_ == 'VB':
            if w.ent_type_:
                ent.append(w.text)
            elif w.lemma_ not in verb and w.text != 'List':
                if w.tag_ == 'NNS':
                    subject.append(w.text[:-1])
                elif w.text == 'year':
                    subject.append('formation')
                else:
                    subject.append(w.text)

    if ent != []:
        name = '%'.join(ent)
        subject.append(name)

    sen = ' '.join(subject)
    res = nlp(sen)
    entity = 'xxx'
    relation = 'xxx'
    for word in res:
        if len(subject) == 3 and 'country' in subject and 'origin' in subject:
            relation = 'origin'
            entity = subject[-1]
        elif len(subject) == 4 and 'country' in subject and 'origin' in subject:
            relation = 'origin'
            entity = subject[-2:-1]
        elif len(subject) > 1 and '%' in subject[1]:
            relation = subject[0]
            entity = subject[1]
        elif word.tag_.startswith('VB') and len(subject) > 1 and not word.dep_ == 'ROOT':
            relation = subject[1]
            entity = subject[0]
        elif len(subject) > 1:
            relation = subject[0]
            entity = subject[1]
        else:
            entity = subject[0]

    if '%' in entity:
        entity = entity.replace('%',' ')

    del subject[:]
    del ent[:]

    '''Comment the next line if you don't want to see what it is searching for'''
    print('{}{}{}'.format([entity],[relation],'#SpaCy'))
    #find_entities(entity, relation)
    return entity, relation