#!/usr/bin/python3

import re

def remove_determiner(word):
    determiners = ["a", "an", "the", "The"]
    word = word.split(" ")
    if len(word) > 1:
        if word[0] in determiners:
            word = " ".join(word[1:])
            return word
        else:
            word = " ".join(word)
            return word
    else: 
        return word[0]


def regular_expression(line):
    entity = "xxx"
    relation = "xxx"
    count = None
    yes_no = None
    while True:
        try:
            query = re.match( r"(Is|Are|Does|Do) (.*) (.*)", line, re.M|re.I) #Who/What is/are the X of Y
            relation, entity = query.group(2), query.group(3)
            entity = remove_determiner(entity)
            yes_no = True
            break
        except:
            pass
        try:
            query = re.match( r"When was (.*) born", line, re.M|re.I) #Who/What is/are the X of Y
            relation, entity = 'birthdate', query.group(1)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"(Who|What) (is|are) the (.*) of (.*)", line, re.M|re.I) #Who/What is/are the X of Y
            relation, entity = query.group(3), query.group(4)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"(Who|What) is (.*)", line, re.M|re.I) #Who/What is X (are erbij)
            relation, entity = "xxx", query.group(2)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"Who founded (.*)", line, re.M|re.I) #Who founded X
            relation, entity = "founder", query.group(1)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"How (many|much) (.*) does (.*) (have|contain)", line, re.M|re.I) #How many/much X does Y have
            relation, entity = query.group(2), query.group(3)
            entity = remove_determiner(entity)
            count = True
            break
        except:
            pass
        try:
            query = re.match( r"How (many|much) (.*) are there in (.*)", line, re.M|re.I) #How many/much X are there in Y
            relation, entity = query.group(2), query.group(3)
            entity = remove_determiner(entity)
            count = True
            break
        except:
            pass

        try:
            query = re.match( r"Count the number of (.*) of (.*)", line, re.M|re.I) #Count the number of X of Y
            relation, entity = query.group(1), query.group(2)
            entity = remove_determiner(entity)
            count = True
            break
        except:
            pass

        try:
            query = re.match( r"(List|Name) the (.*) of (.*)", line, re.M|re.I) #List/Name the X of Y
            relation, entity = query.group(2), query.group(3)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"Give (.*) of (.*)", line, re.M|re.I) #Give X of Y
            relation, entity = query.group(1), query.group(2)
            relation, entity = remove_determiner(relation), remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"Where (does|do|is|are) (.*) (come|originate|originally) from", line, re.M|re.I) #Where does/do X come from?
            relation, entity = "origin", query.group(2)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"(Can|Could|Would|Is|Does|Has|Was|Were|Had|Have|Did|Are|Will) (.*) (a|an) (.*) ", line, re.M|re.I) #Where does/do X come from?
            relation, entity = query.group(4), query.group(2)
            entity = remove_determiner(entity)
            break
        except:
            break

    return entity, relation, count, yes_no


    if '\'' in entity:
        entity = entity[:-2]
    return entity, relation, count

