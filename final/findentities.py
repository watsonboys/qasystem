#!/usr/bin/python3

import requests
import re

sparql = 'https://query.wikidata.org/sparql'
results = []


def find_entities(entity, relation, count):

    api = 'https://www.wikidata.org/w/api.php'
    params = {'action':'wbsearchentities', 'language':'en', 'format':'json'}
    
    if relation == 'description':
        relation = 'xxx'

    if relation == 'xxx':
        params['search'] = entity
        json = requests.get(api,params).json()
        for result in json['search']:
            entity_id = result['id']
            run_query(entity_id, 'xxx', count)

    else:
        params['search'] = entity
        json = requests.get(api,params).json()
        for result in json['search']:
            entity_id = result['id']
            params['search'] = relation
            params['type'] = 'property'
            json = requests.get(api,params).json()
            for result in json['search']:
                relation_id = result['id']
                run_query(entity_id, relation_id, count)

def run_query(entity, relation, count):
    
    num = 0

    if relation == 'xxx':
        query = 'SELECT ?label WHERE { wd:'+entity+' schema:description ?label. FILTER(LANG(?label) = "en")}'
        data = requests.get(sparql, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            for key in item:
                num += 1
                des = item[key]['value']
                results.append(des)
                results.append(num)
        return results

    else: 
        query = 'SELECT ?answerLabel WHERE { wd:'+entity+' wdt:'+relation+' ?answer. SERVICE wikibase:label { bd:serviceParam wikibase:language "en". } }'
        data = requests.get(sparql, params={'query': query, 'format': 'json'}).json()
        
        if count == True:
            if len(data["results"]["bindings"]) > 0:
                results.append(len(data["results"]["bindings"]))
            return results, count     
        
        else:
            for item in data['results']['bindings']:
                for key in item:
                    num += 1
                    if item[key]['type'] == 'literal':
                        results.append(item[key]['value'])
                        results.append(num)

            return results
