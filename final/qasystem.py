#!/usr/bin/python3

import sys
from reg_ex import regular_expression
from find_spacy import *
from findentities import *
from random import randrange

def main(argv):
    infile = open("results","w")
    nlp = spacy.load('en_default')
    q_list = questions(argv)
    for i in q_list:
        print('{}'.format(i))
    print("\nPlease enter your question, formulated as one of the questions above")
    cnt = 0
    for line in sys.stdin:
        cnt += 1
        line = line.strip()
        if line:
            line = line.replace("?","")
            line = line.replace(".","")
            ent, rel, count, yes_no = regular_expression(line)
            if yes_no == True:
                rand = randrange(1,3)
                if rand == 1:
                    results.append('yes')
                else:
                    results.append('no')
            print(line)
            print('{}{}{}'.format([ent],[rel], '#RegEx'))
            if ent == "xxx":
                ent, rel = find_spacy(line, nlp)
                find_entities(ent, rel, count)
            else:
                x = find_entities(ent, rel, count)
                if x == None:
                    ent_s, rel_s = find_spacy(line, nlp)
                    find_entities(ent_s,rel_s,count)
                else:
                    find_entities(ent,rel,count)

        else:
            print("No input, please restart the program.")
            raise SystemExit

        infile = open("results", "a")
        if len(results) == 0:
            string = str(cnt) + "\n"
            infile.write(string)
            print("No answers were found to this question. Please try another one:\n")

        elif 'yes' in results or 'no' in results:
            infile.write('{}\t{}\n'.format(cnt, results[0]))
            print('{}\t{}\n'.format(cnt, results[0]))

        elif len(results) == 1 or count == True:
            infile.write('{}\t{}\n'.format(cnt, results[0]))
            print('{}\t{}\n'.format(cnt, results[0]))
        
        elif len(results) >= 4 and results[1] == results[3]:
            r = []
            it = iter(results)
            [r.append((x,next(it))) for x in it]
            end = []
            for i in range(0, len(r)-1):
                if r[i][1] == r[i + 1][1]:
                    pass
                else:
                    end.append(r[i])
            
            res = []
            for tup in end:
                if tup[0] not in res:
                    res.append(tup[0])
            
            if res == []:
                infile.write('{}\t{}\n'.format(cnt, results[0]))
                print('{}\t{}\n'.format(cnt, results[0]))
            else:
                infile.write('{}\t{}\n'.format(cnt, '\t'.join(res)))
                print('{}\t{}\n'.format(cnt, '\t'.join(res)))
        
        else:
            r = []
            [r.append(x) for x in results if not isinstance(x, int) and x != 'color' and x not in r]
            infile.write('{}\t{}\n'.format(cnt, '\t'.join(r)))
            print('{}\t{}\n'.format(cnt, '\t'.join(r)))

        infile.close()
        del results[:]

def questions(q_list):

    return ["Where does pasta come from?", "What is meat?", "List the main ingredients of bread",
                "What is the country of origin of apple pie?", "What is the E number of citric acid?",
                "Who founded McDonald\'s?","Name the mass of a baguette","What color grapes are there?",
                "How many ingredients does ratatouille have?"]

if __name__ == "__main__":
    main(sys.argv)