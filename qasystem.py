import sys
import spacy
import re
import requests

def main(argv):

    nlp = spacy.load('en_default')
    anchor_text = open('anchor_texts', 'r')
    q_list = questions(argv)
    for i in q_list:
        print('{}'.format(i))
    print("\nPlease enter your question, formulated as one of the questions above")
    
    for line in sys.stdin:
        line = line.strip()
        if line:
            line = line.replace("?","")
            line = line.replace(".","")
            ent, rel, count = regular_expression(line)
            print('[',ent,rel,']')
            if ent == "x" or rel == "x":
                find_spacy(line, nlp)
            else:
                x = find_entities(ent, rel)
                if x == None:
                    find_spacy(line, nlp)

        else:
            print("No input, please restart the program.")
            raise SystemExit

        if len(results) > 2 and results[3] == 1:
            print(results[0])
        elif len(results) == 0:
            print("No answers were found to this question. Please try another one:")
        else:
            no_integers = [x for x in results if not isinstance(x, int)]
            for i in no_integers:
                if i == "color":
                    pass
                else:
                    print(i)
        del results[:]

def questions(q_list):

    return ["What are the ingredients of paella?","List the ingredients of paella.",
            "What ingredients does paella contain?","Who is the founder of McDonald\'s?",
            "Who founded McDonald\'s?","Who is the founder of Mars?",
            "Name the founder of Mars.","By whom was Mars founded?",
            "What color does milk have?","When was the inception of Burger King?"]

def remove_determiner(word):
    determiners = ["a", "an", "the", "The"]
    word = word.split(" ")
    if len(word) > 1:
        if word[0] in determiners:
            word = " ".join(word[1:])
            return word
        else:
            word = " ".join(word)
            return word
    else: 
        return word[0]

sparql = 'https://query.wikidata.org/sparql'
results = []

def regular_expression(line):
    entity = "x"
    relation = "x"
    count = None
    while True:
        try:
            query = re.match( r"(Who|What) (is|are) the (.*) of (.*)", line, re.M|re.I) #Who/What is/are the X of Y
            relation, entity = query.group(3), query.group(4)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"(Who|What) is (.*)", line, re.M|re.I) #Who/What is X (are erbij)
            relation, entity = "Give description", query.group(2)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"Who founded (.*)", line, re.M|re.I) #Who founded X
            relation, entity = "founder", query.group(1)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"How (many|much) (.*) does (.*) (have|contain)", line, re.M|re.I) #How many/much X does Y have
            relation, entity = query.group(2), query.group(3)
            entity = remove_determiner(entity)
            count = True
            break
        except:
            pass
        try:
            query = re.match( r"(List|Name) the (.*) of (.*)", line, re.M|re.I) #List/Name the X of Y
            relation, entity = query.group(2), query.group(3)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"Give (.*) of (.*)", line, re.M|re.I) #Give X of Y
            relation, entity = query.group(1), query.group(2)
            relation, entity = remove_determiner(relation), remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"Where (does|do|is|are) (.*) (come|originate|originally) from", line, re.M|re.I) #Where does/do X come from?
            relation, entity = "origin", query.group(2)
            entity = remove_determiner(entity)
            break
        except:
            pass
        try:
            query = re.match( r"(Can|Could|Would|Is|Does|Has|Was|Were|Had|Have|Did|Are|Will) (.*) (a|an) (.*) ", line, re.M|re.I) #Where does/do X come from?
            relation, entity = query.group(4), query.group(2)
            entity = remove_determiner(entity)
            #count oid meegeven zodat sparql functie weet dat er een ja/nee uit moet komen
            break
        except:
            break

    return entity, relation, count

def find_spacy(line, nlp):

    '''this function makes use of the spacy package, specifically made use of the dependency (.dep_)
    attribute of this package to find dependencies between words.'''
    verb = ['use','come','be','can',
        'could','do','have','may','might','must',
            'shall','would','should','will']
    ent = []
    subject = [] 
    result = nlp(line)
    for w in result:
        if w.tag_.startswith('NN') or w.tag_ == 'VBN' or w.tag_ == 'VB':
            if w.ent_type_:
                ent.append(w.text)
            elif w.lemma_ not in verb and w.text != 'List':
                if w.tag_ == 'NNS':
                    subject.append(w.text[:-1])
                elif w.text == 'year':
                    subject.append('formation')
                else:
                    subject.append(w.text)

    if ent != []:
        name = '%'.join(ent)
        subject.append(name)

    sen = ' '.join(subject)
    res = nlp(sen)
    entity = 'xxx'
    relation = 'xxx'
    for word in res:
        if len(subject) == 3 and 'country' in subject and 'origin' in subject:
            relation = 'origin'
            entity = subject[-1]
        elif len(subject) == 4 and 'country' in subject and 'origin' in subject:
            relation = 'origin'
            entity = subject[-2:-1]
        elif len(subject) > 1 and '%' in subject[1]:
            relation = subject[0]
            entity = subject[1]
        elif word.tag_.startswith('VB') and len(subject) > 1 and not word.dep_ == 'ROOT':
            relation = subject[1]
            entity = subject[0]
        elif len(subject) > 1:
            relation = subject[0]
            entity = subject[1]
        else:
            entity = subject[0]

    if '%' in entity:
        entity = entity.replace('%',' ')

    del subject[:]
    del ent[:]

    '''Comment the next line if you don't want to see what it is searching for'''
    print('{}{}'.format([entity],[relation]))
    find_entities(entity, relation)

def find_entities(entity, relation):

    api = 'https://www.wikidata.org/w/api.php'
    params = {'action':'wbsearchentities', 'language':'en', 'format':'json'}
  
    params['search'] = entity
    json = requests.get(api,params).json()
    for result in json['search']:
        entity_id = result['id']
        params['search'] = relation
        params['type'] = 'property'
        json = requests.get(api,params).json()
        for result in json['search']:
            relation_id = result['id']
            run_query(entity_id, relation_id)

sparql = 'https://query.wikidata.org/sparql'
results = []

def run_query(entity, relation):

    num = 0
    query = 'SELECT ?answerLabel WHERE { wd:'+entity+' wdt:'+relation+' ?answer. SERVICE wikibase:label { bd:serviceParam wikibase:language "en". } }'
    data = requests.get(sparql, params={'query': query, 'format': 'json'}).json()
    for item in data['results']['bindings']:
        for key in item:
            num += 1
            if item[key]['type'] == 'literal':
                results.append(item[key]['value'])
                results.append(num)

if __name__ == "__main__":
    main(sys.argv)