#!/usr/bin/python3
#Project Taaltechnologie 1617
#Group 5 - Team Watsonboys
#Luuk Looijenga, Mike Zhang, Kamil Zukowski en Leon Graumans

import sys
import spacy
import requests


nlp = spacy.load("en")
url = "https://www.wikidata.org/w/api.php"
params = {"action":"wbsearchentities","language":"en","format":"json"}
sparql = "https://query.wikidata.org/sparql"


def main(argv):
	for line in sys.stdin:
		line = line.rstrip()
		answer = create_and_fire_query(line) 
		print(answer)


def fire_sparql(ent, rel):
	query = "SELECT ?itemLabel WHERE {?var wdt:"+ rel +"? wd:"+ ent +"; wdt:"+ rel +" ?item. SERVICE wikibase:label {bd:serviceParam wikibase:language 'en' .}}"
	data = requests.get(sparql, params={"query": query, "format": "json"}).json()
	answer = []
	for item in data["results"]["bindings"]:
		for var in item :
			answer.append(item[var]["value"])
	return answer

if __name__ == "__main__":
	main(sys.argv)
