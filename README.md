# README #

QA System like Watson.

### Members ###

- Leon Graumans
- Luuk Looijenga
- Mike Zhang
- Kamil Zukowski

### Division of tasks ###

- Leon Graumans: Regex
- Luuk Looijenga: Wikidata, anchor texts
- Mike Zhang: SpaCy
- Kamil Zukowski: counts, yes/no

### Read me ###