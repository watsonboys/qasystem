#!/usr/bin/python3
#Project Taaltechnologie 1617
#Group 5 - Team Watsonboys
#Luuk Looijenga, Mike Zhang, Kamil Zukowski en Leon Graumans

import re
import sys
import requests

def main(argv):
	for line in sys.stdin:
		line = line.rstrip()
		line = line.rstrip("?")
		line = line.rstrip(".")
		ent, rel, count = regular_expression(line) 
		print("ent\t",ent)
		print("rel\t",rel)
		if count == True:
			print("Geef me een lijstje pls")

def remove_determiner(word):
	determiners = ["a", "an", "the", "The"]
	word = word.split(" ")
	if len(word) > 1:
		if word[0] in determiners:
			word = " ".join(word[1:])
			return word
		else:
			word = " ".join(word)
			return word
	else: 
		return word[0]

def regular_expression(line):
	entity = ""
	relation = ""
	count = None
	while True:
		try:
			query = re.match( r"(Who|What) (is|are) the (.*) of (.*)", line, re.M|re.I) #Who/What is/are the X of Y
			relation = query.group(3)
			entity = query.group(4)
			entity = remove_determiner(entity)
			break
		except:
			pass
		try:
			query = re.match( r"(Who|What) is (.*)", line, re.M|re.I) #Who/What is X (are erbij)
			relation = "Give description"
			entity = query.group(2)
			entity = remove_determiner(entity)
			break
		except:
			pass
		try:
			query = re.match( r"Who founded (.*)", line, re.M|re.I) #Who founded X
			relation = "founder"
			entity = query.group(1)
			entity = remove_determiner(entity)
			break
		except:
			pass
		try:
			query = re.match( r"How (many|much) (.*) does (.*) (have|contain)", line, re.M|re.I) #How many/much X does Y have
			relation = query.group(2)
			entity = query.group(3)
			entity = remove_determiner(entity)
			count = True
			break
		except:
			pass
		try:
			query = re.match( r"(List|Name) the (.*) of (.*)", line, re.M|re.I) #List/Name the X of Y
			relation = query.group(2)
			entity = query.group(3)
			entity = remove_determiner(entity)
			break
		except:
			pass
		try:
			query = re.match( r"Give (.*) of (.*)", line, re.M|re.I) #Give X of Y
			relation = query.group(1)
			relation = remove_determiner(relation)
			entity = query.group(2)
			entity = remove_determiner(entity)
			break
		except:
			pass
		try:
			query = re.match( r"Where (does|do|is|are) (.*) (come|originate|originally) from", line, re.M|re.I) #Where does/do X come from?
			relation = "origin"
			entity = query.group(2)
			entity = remove_determiner(entity)
			break
		except:
			pass
		try:
			query = re.match( r"(Can|Could|Would|Is|Does|Has|Was|Were|Had|Have|Did|Are|Will) (.*) (a|an) (.*) ", line, re.M|re.I) #Where does/do X come from?
			relation = query.group(4)
			entity = query.group(2)
			entity = remove_determiner(entity)
			#count oid meegeven zodat sparql functie weet dat er een ja/nee uit moet komen
			break
		except:
			break


	return entity, relation, count

"""Fire sparql query, als count True is, dan geeft ie het aantal resultaten ipv de resultaten zelf"""
def fire_sparql(ent, rel, count): #let erop, hier komt er nog een count mee
	query = "SELECT ?itemLabel WHERE {?var wdt:"+ rel +"? wd:"+ ent +"; wdt:"+ rel +" ?item. SERVICE wikibase:label {bd:serviceParam wikibase:language 'en' .}}"
	data = requests.get(sparql, params={"query": query, "format": "json"}).json()
	if count != True:
		for item in data["results"]["bindings"]:
			for var in item :
				print(item[var]["value"])
	elif count == True:
		print(len(data["results"]["bindings"]))


if __name__ == "__main__":
	main(sys.argv)
