#!/usr/bin/python3
#
# File: s2988240.py
# Date: 19/05/2017
# Revision: 3
# Author: Jun Jun Zhang s2988240
#
# Revision #1: Made program s2988240.py
# Revision #2: Added function find_spacy(), deleted create_valid_query(), remove_det()
# Revision #3: Added missing combinations like: "List Jamie Oliver's birthday."
#
# User can input a question related to food,
# the program tries to give an answer back.

import sys
import requests
import re
import spacy

sparql = 'https://query.wikidata.org/sparql'
results = []

def anc_text():
	txt = open('anchor_texts')
	return txt

def print_example_queries():
	'''prints 10 example queries'''
	print("\nExample queries related to food:\n")
	print("- What is the origin of spaghetti?")
	print("- What colour does milk have?")
	print("- When is Jamie Oliver's birthday?")
	print("- When is the birthday of Gordon Ramsay?")
	print("- What shape does a croissant have?")
	print("- List the ingredients of paella.")
	print("- Who is the founder of Guinness?")
	print("- Where is the HQ of Campina?")
	print("- What are the ingredients of paella?")
	print("- What is the origin of Segafredo Zanetti?")
	print("- List Jamie Oliver's birthday.")
	print('\n{}\n{}\n{}\n{}'.format('Ask your own question!','These structures are covered:','- \'Who/What/When is/are the X of Y?\'','- \'When/What is Y\'s X?\''))
	print('{}\n{}'.format('- \'What X does Y have?\'','- \'List X of Y\''))
	print('\n{}\n{}\n'.format('NOTE: It may take a while','(Press Enter to quit)'))

def find_spacy(line, anchor_text, nlp):
	'''this function makes use of the spacy package, specifically made use of the dependency (.dep_)
	attribute of this package to find dependencies between words.'''
	verb = ['use','come','be','can',
		'could','do','have','may','might','must',
			'shall','would','should','will']
	ent = []
	subject = [] 
	result = nlp(line)
	for w in result:
		if w.tag_.startswith('NN') or w.tag_ == 'VBN' or w.tag_ == 'VB':
			if w.ent_type_:
				ent.append(w.text)
			elif w.lemma_ not in verb and w.text != 'List':
				if w.tag_ == 'NNS':
					subject.append(w.text[:-1])
				elif w.text == 'year':
					subject.append('formation')
				else:
					subject.append(w.text)


	if ent != []:
		name = '%'.join(ent)
		subject.append(name)


	sen = ' '.join(subject)
	res = nlp(sen)
	entity = 'xxx'
	relation = 'xxx'
	for word in res:
		if len(subject) == 3 and 'country' in subject and 'origin' in subject:
			relation = 'origin'
			entity = subject[-1]
		elif len(subject) == 4 and 'country' in subject and 'origin' in subject:
			relation = 'origin'
			entity = subject[-2:-1]
		elif len(subject) > 1 and '%' in subject[1]:
			relation = subject[0]
			entity = subject[1]
		elif word.tag_.startswith('VB') and len(subject) > 1 and not word.dep_ == 'ROOT':
			relation = subject[1]
			entity = subject[0]
		elif len(subject) > 1:
			relation = subject[0]
			entity = subject[1]
		else:
			entity = subject[0]

	if '%' in entity:
		entity = entity.replace('%',' ')

	del subject[:]
	del ent[:]

	ent_id = create_anchor_id(entity, anchor_text)
	'''Comment the next line if you don't want to see what it is searching for'''
	print('{}{}'.format([entity],[relation]))
	create_and_fire_query_method(relation, ent_id, entity)

def create_anchor_id(ent,anchor_text):
	'''This function looks the entity up in the anchor texts and returns the ID of the entity''' 
	page=['uri',0]
	for texts in anchor_text:
		if str(ent) in texts:
			texts = texts.split('\t')
			if int(texts[2]) > page[1]:
				page[0] = texts[1]
				page[1] = int(texts[2])
	
	if page[0] != 'uri':
		url = page[0].replace('>','')
		url = url.replace('<https://en.wikipedia.org/wiki/', 'https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&titles=')
		data = requests.get(url).json()
		id_ent = data['query']['pages'].keys()
		anchor_id = data['query']['pages'][list(id_ent)[0]]['pageprops']['wikibase_item']
		return anchor_id
	else:
		return False

def create_and_fire_query_method(relation, anchor_id, entity):
	'''Making use of the method given in the lecture, this function makes
	use of the wikidata.org API and also gives the ID of the entity and relation'''
	api = 'https://www.wikidata.org/w/api.php'
	params = {'action':'wbsearchentities', 'language':'en', 'format':'json'}

	params['search'] = entity
	json_entity = requests.get(api, params).json()
	for result in json_entity['search']:
		api_id = result['id'] 
		params['search'] = relation
		params['type'] = 'property'
		json_relation = requests.get(api,params).json()
		for result in json_relation['search']:
			relation_id = result['id']
			if anchor_id == api_id:								# Here it checks whether the anchor_id is the same as the id from the api, 
				fire_sparql(anchor_id, relation_id)				# it wil use the anchor_id if it is true, otherwise we use the api_id.
			else:
				fire_sparql(api_id, relation_id)


def fire_sparql(ent,rel):
	'''This function makes use of the database query langauge.
	To find the answer related to the entity'''

	num = 0
	query = 'SELECT ?answerLabel WHERE { wd:'+ent+' wdt:'+rel+' ?answer. SERVICE wikibase:label { bd:serviceParam wikibase:language "en".} }'
	print(query)
	data = requests.get(sparql, params={'query': query, 'format': 'json'}).json()
	if data['results']['bindings'] != []:
		for item in data['results']['bindings']:
			for key in item:
				num += 1
				if item[key]['type'] == 'literal':
					results.append(item[key]['value'])
					results.append(num)
				else:
					print('{}'.format(item[key]))
	else:
		return False

def main(argv):

	if len(argv) > 1 :
		print("Usage: ./s2988240.py", file = sys.stderr)
		exit(-1)

	anchor_text = anc_text()
	nlp = spacy.load('en_default')
	print_example_queries()
	while True:
		try:
			cnt = 1
			for line in sys.stdin:
				line = line.replace('?','').rstrip()
				if line:
					find_spacy(line,anchor_text,nlp)

					if len(results) == 0:
						print('{} {}'.format(cnt, 'Could not find an answer, please try another question.'))

					elif len(results) > 2 and results[3] == 1:
						print('{} {}'.format(cnt, results[0]))
						
					else:
						multiple_res = [x for x in results if not isinstance(x, int)]
						for item in multiple_res:
							print('{} {}'.format(cnt, item))
					cnt +=1
					print("")
					del results[:]

				else:
					print('These were the answers.')
					print('No answers? Please restart and try again.')
					txt.close()
					return False
		except:
			return False


if __name__ == "__main__":
	main(sys.argv)